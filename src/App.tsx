import { useEffect, useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'
import useConsulta from './hooks/customer'
import Spinner from './utils/loader/loader'
import UserCard from './utils/userCard/userCard'
function App() {
  const [count, setCount] = useState(0)
  const url='https://rickandmortyapi.com/api/character'
  const [bodyUser,setBodyUser]= useState(<></>)
  const {data,error,loading}= useConsulta(url)
 
  useEffect(()=>{
    loadData()
  },[loading])
  const loadData=()=>{
    const row=[]
    if(data.length>0){
      
      for (let i=0;i<data.length;i=i+3) {
        row.push(
          <div className='containerCards'>
              <UserCard name={data[i].name} image={data[i].image}/>
              {i+1< data.length&&<UserCard name={data[i+1].name} image={data[i+1].image}/>}
              {i+2< data.length&&<UserCard name={data[i+2].name} image={data[i+2].image}/>}
          </div>
        )
        
      }
      setBodyUser(row)
    }
    
    
  }


  if(loading){ return(
      <>
        <Spinner />
      </>
    )
  }else if(error.length>0){
      alert('Se ha resentado un error')
  }else{
      
    return (
      <>
        
        {bodyUser}
      </>
    )
  }
}

export default App

import { useEffect, useState } from "react"



const useConsulta = (url) => {
    
    const [data,setData]=useState([])
    const [error,setError]=useState([])
    const [loading,setLoading]=useState(true)
    

    useEffect(()=>{
        getInformation()
    },[])
    const getInformation=async()=>{
        

          try{
            const response = await fetch(url);
            //console.log("🚀 ~ file: customer.ts:48 ~ getInformation ~ response:", response)
            const data = await response.json();
            //console.log("🚀 ~ file: customer.ts:22 ~ getInformation ~ data:", data)
            setLoading(false)
            if(!response?.ok){
                return setError(data)
            } 
            setData(data?.results)
          }catch(error){
            setLoading(false)
             setError(error)
          }

    }
    return{data,error,loading}

}

export default useConsulta;
import { useEffect, useState } from 'react'
import "./styles.css";


const UserCard=(data) => {
    console.log("🚀 ~ file: userCard.tsx:6 ~ UserCard ~ data:", data)
   
    return(
        <>
            <div className='container'>
                <img src={data.image} className="imageUser" alt="Vite logo" />
                <p>{data.name}</p> 
            </div>
        </>
    )

}

export default UserCard